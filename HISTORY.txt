SSH into instance
  ssh -i ssh/key_ec2 ec2-user@<PUBLIC_IP>

Install docker via
  sudo amazon-linux-extras install docker

Start docker via 
  sudo service docker start

Start the app
  sudo docker run -t -i -p 80:3000  registry.gitlab.com/gitlab-org/creator-pairing/5-minute-prod-app/rails-app/master

With database url it will be like
  sudo docker run -t -i -p 80:3000 \
    -e DATABASE_URL="<DB_URL_HERE>" \
    registry.gitlab.com/gitlab-org/creator-pairing/5-minute-prod-app/rails-app/master
